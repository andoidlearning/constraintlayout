package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

//import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn : Button = findViewById(R.id.btn_submit)
        val firstName : EditText = findViewById(R.id.firstname_edtText)
        val lastName : EditText = findViewById(R.id.lastname_edtText)
        val dob : EditText = findViewById(R.id.dob_edtText)
        val country : EditText = findViewById(R.id.country_edtText)

        btn.setOnClickListener {
            val firstNameText = firstName.text.toString()
            val lastNameText = lastName.text.toString()
            val dobText = dob.text.toString()
            val countryText = country.text.toString()

            Log.d("MainActivity", "$firstNameText, $lastNameText, $dobText, $countryText")
        }
    }
}